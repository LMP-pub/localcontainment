# README

See LICENSE file for code and data licensing information.

This repository contains code written in [`Julia`](https://julialang.org/) v1.4
to reproduce the data analysis and simulations
of the manuscript "Local measures enable COVID-19 containment with fewer
restrictions due to cooperative effects" by Philip Bittihn, Lukas Hupe\*,
Jonas Isensee\* and Ramin Golestanian published [here](https://doi.org/10.1016/j.eclinm.2020.100718).

You can either download a zipped package from the [gitlab page](https://gitlab.gwdg.de/LMP-pub/localcontainment)
of the repository or clone the code from the command line via
```bash
git clone https://gitlab.gwdg.de/LMP-pub/localcontainment.git
```
into a directory of your choice.

The estimation of the reproduction number during lockdown (folder `r_estimation`)
and the simulations (folder `simulations`) are completely independent. If you are
new to Julia, note that the plotting dependencies for the R estimation are quite
heavy and could take a long time to install and precompile, while the simulations
are more light-weight.


## Notes on R Estimation
The folder `r_estimation` contains code that was used for computing estimates
of the reproduction number *R* for the stochastic simulations.

Sources for the infection numbers are cited in the main text but
the code also contains automatic download links to get the latest data.

Once the files are ready and saved in the `r_estimation` folder the software
dependencies need to be installed. Most notably the `R` programming language
and the library [`EpiEstim`](https://www.rdocumentation.org/packages/EpiEstim/versions/2.2-1)
that is used making time-resolved estimates of the reproduction number *R*.

The code however is written in [`Julia`](https://julialang.org/) v1.4.
All further dependencies can be installed by executing

```julia
import Pkg
cd("/path/to/r_estimation/")
Pkg.activate(".")
Pkg.instantiate()
```

Executing the scripts via
```julia
include("ger_r_estim.jl")  # Example for Germany
```
prints the best estimate of the lockdown *R* and displays a figure with the
history of *R*, which is also saved to a pdf file.


## Notes on simulations
The folder `simulations` contains the code necessary to reproduce the
simulations. It is completely written in [`Julia`](https://julialang.org/) v1.4.
All dependencies, including the [stochastic simulation framework](https://gitlab.gwdg.de/LMP-pub/GeneralizedStochasticSimulations.jl)
developed by our group, can be installed by executing
```julia
import Pkg
cd("/path/to/simulations/")
Pkg.activate(".")
Pkg.instantiate()
```

An example simulation is contained in the script `simulation_example.jl`, which can
be executed via
```julia
include("simulation_example.jl")
```
It prints the number of days in lockdown for the average person for a single
simulation with the parameters specified in the script. Further details
on the simulations, parameters, etc., can be found in the comments in the
script file.
