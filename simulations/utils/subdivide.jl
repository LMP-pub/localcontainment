
"""
    subdividefurther!(rng, allparams, subdivsize)

Subdivides the regions defined in the `allparams` dictionary further by
splitting them into `n` equally sized regions, with `n` sufficiently large to
reach a population size `≦ subdivsize`. The population sizes are distributed
equally between the new regions using [`distributeevenly`](@ref), the number of
infected and removed individuals are distributed randomly between the resulting
regions using [`distributerandomly`](@ref) and the random number generator
`rng`.

Returns the "subdivision map", i.e. a vector of indices with length equal to the
number of resulting regions, and each element indicating the index of the region
it originated from by splitting.
"""
function subdividefurther!(rng, allparams, subdivsize)
    subdivisionmap = Vector{Int64}()
    subsizes = Vector{Int64}()
    subinfected = Vector{Int64}()
    subremoved = Vector{Int64}()
    for (i,s) in enumerate(allparams[:subsizes])
        numsub = 1
        while s/numsub>subdivsize
            numsub += 1
        end
        append!(subdivisionmap,fill(i,numsub))
        append!(subsizes,distributeevenly(s,numsub))
        append!(subinfected,distributerandomly(rng, allparams[:subinfected][i],numsub))
        append!(subremoved,distributerandomly(rng, allparams[:subremoved][i],numsub))
    end
    allparams[:subsizes] = subsizes
    allparams[:subinfected] = subinfected
    allparams[:subremoved] = subremoved
    return subdivisionmap
end