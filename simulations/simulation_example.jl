using Random
using Printf
using DataStructures
using JSON3


using GeneralizedStochasticSimulations

### Utility function for setting up the subdivision
include("utils/subdivide.jl")

### Load the two stochastic models
include("models/globalleakinesslocallockdown.jl")
include("models/globalleakinessgloballockdown.jl")

############################################################
### Choose one model
############################################################
### Regional lockdown strategy
model = GlobalLeakinessLocalLockdown.Population
### Global lockdown strategy
# model = GlobalLeakinessGlobalLockdown.Population
############################################################


############################################################
### Generate initial conditions (only necessary for U.S.)
############################################################
### For states in the U.S., the data necessary for the
### initial condition is downloaded on the fly and the
### file is generated on the fly
### (because we are not allowed to distribute Johns Hopkins
### data with our code)
# include("countrydata/us_ic_gen.jl")
############################################################



############################################################
### Select country by uncommenting corresponding lines
### (for states in the U.S. generate file above first)
############################################################
popdatafile = "countrydata/ger_ic_2020-06-12.json"
lockdownR = 0.6849

# popdatafile = "countrydata/ita_ic_2020-06-12.json"
# lockdownR = 0.7127

# popdatafile = "countrydata/en_ic_2020-06-12.json"
# lockdownR = 0.7224

# popdatafile = "countrydata/us_New_York_ic_2020-06-12.json"
# lockdownR = 0.6984

# popdatafile = "countrydata/us_Florida_ic_2020-06-12.json"
# lockdownR = 0.8641
############################################################


### Load population data
popdata = JSON3.open(JSON3.read,popdatafile)

### individual regions are further subdivided until
### they are below this size (Inf = no subdivision)
subdivsize = 200000 


randomseed = 1234

k = 0.14

params = OrderedDict(
    :k => k,             # SIR model parameter k
    :b => 0.16,          # SIR model parameter b (R0 = b/k)
    :ξ => 0.01,          # leakiness (proportion of cross-regional contacts)
    
    #Choose relative or absolute threshold
    :θ => 10/100000,     # relative lockdown threshold as fraction of population
    #:absthresh => 10,   # absolute lockdown threshold in each subpop
    
    :τ => 14,            # days over threshold before initiating lockdown
    :τsafety => 21,      # days under threshold before lifiting lockdown 

    :subsizes => Vector{Int64}(popdata[:population]),  # Sub-population sizes
    :subinfected => Vector{Int64}(popdata[:active]),   # Infected in each subpop
    :subremoved => Vector{Int64}(popdata[:recovered]), # Removed in each subpop
    :blockdown => k*lockdownR,                         # b during lockdown

)

### Set up the simulation

rng = MersenneTwister(randomseed)

println("Originally ", length(params[:subsizes]), " subpops with a population of ", sum(params[:subsizes]))
# for each resulting subpopulation, subdivisionmap contains the index of the 
# subpopulation from which it was split up.
subdivisionmap = subdividefurther!(rng, params, subdivsize)
println("Split into ", length(params[:subsizes]), " subpops with a population of ", sum(params[:subsizes]))


### Convert θ to absolute thresholds for each sub-population
if haskey(params,:θ)
    θ = pop!(params, :θ)
    params[:Ithreshup] = ceil.(Int64, θ*params[:subsizes])
else
    absthresh = pop!(params, :absthresh)
    params[:Ithreshup] = fill(absthresh, length(params[:subsizes]))
end

### If using global lockdown model, sum up all the individual absolute
### thresholds to obtain total threshold.
if model == GlobalLeakinessGlobalLockdown.Population
    params[:Ithreshup] = sum(params[:Ithreshup])
end



### Record states every day for 5 years
Δtsample = 1.
tarray = collect(0:Δtsample:5*365)

### setup system
population = model(;params...)

### For the densely connected model with all-to-all leakiness
### EvolveNaiveState (essentially direct Gillespie) is the most efficient exact algorithm
### (see docs in GeneralizedStochasticSimulations for more details)
evstate = EvolveNaiveState(population)

### The actual simulation
### (see docs in GeneralizedStochasticSimulations for more details)
states, t, bytes, gctime, memallocs = @timed evolveandrecordstates!(rng, evstate, population, tarray)

println("Simulation terminated ($t s)")

### Calculate days in lockdown for the average person
### (inspect the "states" variable for more details)
daysinlockdown = sum(sum(getindex.(states,"isinlockdown")).*params[:subsizes])/sum(params[:subsizes])*Δtsample

println("Expected days in lockdown: ", daysinlockdown)

