using DataFrames, CSV, Query, JSON3, Dates


############################################################
### Generate initial conditions (only necessary for U.S.)
############################################################
### For states in the U.S., the data necessary for the
### initial condition is downloaded on the fly and the
### file is generated on the fly
### (because we are not allowed to distribute Johns Hopkins
### data with our code)
############################################################


"""
    us_ic_gen(state, cutoffdate)
Generate initial condition for state `state` in the U.S. by downloading
Johns Hopkins data for infection numbers and census bureau data for populations.
Saves the data in a file next to this source code file.
    
# Required parameters

 * `state`: String indicating the U.S. state to extract, e.g. "Florida"

## Optional parameters

 * `cutoffdate`: Cut off data after the specified `Date(...)`
    default value: `Date(2020,06,12)`
""" 
function us_ic_gen(state, cutoffdate = Date(2020,06,12))




download("https://raw.githubusercontent.com/CSSEGISandData/COVID-19/master/csse_covid_19_data/csse_covid_19_time_series/time_series_covid19_confirmed_US.csv",
    joinpath(@__DIR__, "us_data_latest.csv"))
loaded_df = CSV.read(joinpath(@__DIR__, "us_data_latest.csv"))
loaded_df = stack(loaded_df, 12:ncol(loaded_df))
loaded_df.variable = Date.(String.(loaded_df.variable), dateformat"mm/dd/yy").+Year(2000)
gdf = groupby(loaded_df, [:Admin2, :Combined_Key, :FIPS, :Province_State])

cmbnd = combine(gdf, :value =>  :cases, :variable => :date)
cmbnd[!, :countyname] = cmbnd.Admin2 .* " County"
filter!(r-> r.Province_State == state, cmbnd)
filter!(r-> Date(2020, 03, 01) <= r.date <= cutoffdate, cmbnd)
################################################################################
#             Compute currently infected and already recovered                 #
################################################################################
# Could not find any data on recoveries
# Will therefore assume that all cases more than 14 days ago will count as
# recovered


gdf = groupby(cmbnd, :countyname)
case_data = combine(gdf) do df
    active    = df.cases[end]-df.cases[end-14]
    recovered = df.cases[end-14]
    if ismissing(recovered)
        println(df.cases)
    end
    (active=active, recovered=recovered)
end

################################################################################
#                             Load Population data                             #
################################################################################

if !isfile("us_population_data.csv")
    download("https://www2.census.gov/programs-surveys/popest/datasets/2010-2019/counties/totals/co-est2019-alldata.csv",
            joinpath(@__DIR__,"us_population_data.csv"))
end

popdf = CSV.read(joinpath(@__DIR__, "us_population_data.csv"))
select!(popdf, :STNAME, :CTYNAME, :POPESTIMATE2019, :COUNTY)
# Filter out entry for whole state
popdf = filter(r -> r.COUNTY != 0 && r.STNAME == state, popdf)

# Is complete for New York
region_data = innerjoin(popdf, case_data, on=:CTYNAME=>:countyname)

################################################################################
#                            Wrap into JSON file                               #
################################################################################

data = Dict(:region => region_data.CTYNAME,
            :active => region_data.active,
            :recovered => region_data.recovered,
            :population => region_data.POPESTIMATE2019)


#


if state == "New York"
    println("New York: redistributing")
    
    # Redistribute NYC infections because they have all been lumped into Manhattan
    NYCcounties = collect(findall(occursin.(name,data[:region]))[] for name in ["Bronx", "Kings", "Queens", "Richmond", "New York"])
    NYcounty = findall(occursin.("New York",data[:region]))[]

    println("Before redistribution:")
    for s in [:region, :population, :active, :recovered]
        println(string(s), ": ", data[s][NYCcounties])
    end

    for s in [:active, :recovered]
        totalperpop = (data[s][NYcounty]/sum(data[:population][NYCcounties]))
        data[s][NYCcounties] = round.(Int,totalperpop*data[:population][NYCcounties])
    end

    println("After redistribution:")
    for s in [:region, :population, :active, :recovered]
        println(string(s), ": ", data[s][NYCcounties])
    end
end


open(joinpath(@__DIR__, "us_$(replace(state, ' ' => '_'))_ic_$(cutoffdate).json"), "w") do io
    JSON3.write(io, data)
return
end

end

us_ic_gen("Florida")
us_ic_gen("New York")

