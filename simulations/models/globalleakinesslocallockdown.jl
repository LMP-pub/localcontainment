module GlobalLeakinessLocalLockdown

# Include the framework
# (for more details on the simulation logic and setup of this model definition,
# see docs of the GeneralizedStochasticSimulations package)
using GeneralizedStochasticSimulations
using DataStructures

# Struct holding the size and state of a sub-population
# The type R signifies a reactant.
mutable struct Group{R<:Reactant}
    N::Int64
    S::R
    I::R
    isinlockdown::R
    lockdownevent::Int64
    liftlockdownevent::Int64
    Ithreshup::Int64
    Ithreshdown::Int64
end

# Infection reaction with a reference to a specific group in which the infection
# takes place (rates and effects of reactions are defined further below via the
# @rate and @reaction macros)
struct Infection{R<:Reactant} <: AbstractReaction
    group::Group{R}
end

# Recovery reaction with a reference to a specific group in which the recovery
# takes place (rates and effects of reactions are defined further below via the
# @rate and @reaction macros)
struct Recovery{R<:Reactant} <: AbstractReaction
    group::Group{R}
end

# Event signifying an activation of a local lockdown with a reference to a
# specific group. Effects of an event are defined further below via the @event
# macro and scheduling of events takes place inside other reactions.
struct ActivateLockdown{R<:Reactant} <: AbstractReaction
    group::Group{R}
end

# Event signifying a removal of a local lockdown with a reference to a specific
# group. Effects of an event are defined further below via the @event macro and
# scheduling of events takes place inside other reactions.
struct LiftLockdown{R<:Reactant} <: AbstractReaction
    group::Group{R}
end

"""
    GlobalLeakinessLocalLockdown.Population
The reaction system struct storing the parameters and population-wide variables
of the system. Constructor (keyword) parameters are:

*   `subinfected`: vector specifying the number of infected in each sub-population
*   `subsizes`: vector specifying the size of each sub-population
*   `subremoved`:
    vector specifying the number of removed (recovered) individuals in each
    sub-population (defaults to `zeros(Int64,size(subsizes)`)
*   `b`: SIR model parameter b
*   `k`: SIR model parameter k
*   `ξ`: leakiness
*   `blockdown`: parameter b during lockdown
*   `τ`:
    delay for the initiation of lockdown. Number of infections must be ≧ the
    respective element of `Ithreshup` for a period `τ` to iniate lockdown.
*   `τsafety`:
    delay for lifting lockdown (defaults to `τ`). Number of infections must be ≦
    the respective element of `Ithreshdown` for a period `τsafety` to lift
    lockdown.
*   `Ithreshup`:
    vector of thresholds for initiating lockdown for each sub-population (number
    of infections in that sub-populations)
*   `Ithreshdown`:
    vector of thresholds for lifting lockdown for each sub-population (defaults
    to `Ithreshup .- 1`)
"""
mutable struct Population{R<:Reactant} <: AbstractReactionSystem{R}
    t::Float64
    
    b::Float64
    ξ::Float64
    k::Float64
    
    
    q::Float64
    τ::Float64
    τsafety::Float64
    
    Ntotal::Int64
    Itotal::R
    Itotallockdown::R
    
    groups::Vector{Group{R}}
    reactions::Vector{AbstractReaction}
    events::Vector{AbstractReaction}
    network::Vector{Vector{Int64}}
    eventstoreactions::Vector{Vector{Int64}}
    #reactiontoevents::Vector{Vector{Int64}}
    
    scheduledevents::PriorityQueue{Int64,Float64}
    
    function Population{R}(; t = 0., b, ξ, k, blockdown, τ, τsafety=τ,
                                        Ithreshup, Ithreshdown=Ithreshup .- 1, subinfected::AbstractVector{Int64},
                                        subsizes::AbstractVector{Int64}, subremoved = zeros(Int64,size(subsizes))) where {R<:Reactant}
        # Works no matter whether Ithreshup was a scalar or already an array
        # but throws an error if the number of elements don't match
        IthreshupVec = similar(subsizes)
        IthreshupVec[:] .= Ithreshup
        
        # Same for Ithreshdown
        IthreshdownVec = similar(subsizes)
        IthreshdownVec[:] .= Ithreshdown
        
            
        groups = [Group{R}(N, N-I0-R0, I0, (I0 >= tup ? 1 : 0), -1, -1,tup,tdown) for (N,I0,R0,tup,tdown) ∈ zip(subsizes,subinfected,subremoved,IthreshupVec,IthreshdownVec)]
        reactions = [kind(g) for (kind,g) ∈ Base.Iterators.product([Infection{R} Recovery{R}],groups)][:]
        events = Vector{AbstractReaction}()
        for (i,g) in enumerate(groups)
            push!(events,ActivateLockdown{R}(g))
            g.lockdownevent = length(events)
            push!(events,LiftLockdown{R}(g))
            g.liftlockdownevent = length(events)
        end
        Ntotal = sum(subsizes)
        Itotal = sum(Vector{Int64}([g.I for g in groups if g.isinlockdown==0]))
        Itotallockdown = sum(Vector{Int64}([g.I for g in groups if g.isinlockdown==1]))
        return new{R}(t,b,ξ,k, sqrt(blockdown/b), τ, τsafety, 
                      Ntotal, Itotal, Itotallockdown, groups, reactions, events, [], [],
                      PriorityQueue{Int64,Float64}())
    end
end

# Convenience constructor calculating topology etc. See docs of
# GeneralizedStochasticSimulations for details.
function Population(;kwargs...)
    sp = Population{Int64}(;kwargs...)
    sp.network = topology(Population; kwargs...)
    sp.eventstoreactions = topology(Population; sourcereactionsfunc=x->x.events, kwargs...)
    #sp.reactiontoevents = topology(Population; sourcereactionsfunc=getreactions, targetreactionsfunc=getevents, kwargs...)
    
    return sp
end

GeneralizedStochasticSimulations.getnexteventtime(rs::Population) = isempty(rs.scheduledevents) ? Inf : first(rs.scheduledevents)[2]
GeneralizedStochasticSimulations.popnextreaction!(rs::Population) = dequeue!(rs.scheduledevents)

# Definition of the infection reaction, increasing I and decreasing S in the
# respective group. At the same time, the total number of infections in the
# population (or the total number of infections in lockdown) is updated.
# Initiation of lockdown is scheduled or the removal of lockdown is descheduled
# as appropriate.
@reaction Population Infection begin
    g = r.group
    g.S -= 1
    g.I += 1
    if g.isinlockdown==0
        s.Itotal += 1
    else
        s.Itotallockdown += 1
    end
    if g.I == g.Ithreshup
        enqueue!(s.scheduledevents, g.lockdownevent, s.t + s.τ)
        # if r.group.lockdownevent==1
        #     println("I=", r.group.I, " t=", s.t, "Lockdown event ", r.group.lockdownevent,
        #     " scheduled for ", s.t + s.τ)
        # end
    end
    if g.I == g.Ithreshdown+1
        if haskey(s.scheduledevents, g.liftlockdownevent)
            dequeue!(s.scheduledevents, g.liftlockdownevent)
            # if r.group.lockdownevent==1
            #     println("I=", r.group.I, " t=", s.t, "LiftLockdown event ", r.group.liftlockdownevent,
            #     " de-scheduled")
            # end
        end
    end
    return true
end r.group.I r.group.S s.Itotal s.Itotallockdown

# Rate of the infection reaction in a group
@rate Population Infection begin
    qstar = (r.group.isinlockdown == 0 ? 1. : s.q)
    return  s.b*qstar*r.group.S*
              ( (1-s.ξ) * qstar *      r.group.I/r.group.N +
                  s.ξ   *        (s.Itotal+s.q*s.Itotallockdown)/s.Ntotal
              )
end r.group.isinlockdown r.group.I r.group.S s.Itotal s.Itotallockdown

# Definition of the recovery reaction, decreasing I in the respective group. At
# the same time, the total number of infections in the population (or the total
# number of infections in lockdown) is updated. Initiation of lockdown is
# descheduled, or the removal of lockdown is scheduled when appropriate.
@reaction Population Recovery begin
    g = r.group
    g.I -= 1
    if g.isinlockdown==0
        s.Itotal -= 1
    else
        s.Itotallockdown -= 1
    end
    if g.I == g.Ithreshup-1
        if haskey(s.scheduledevents, g.lockdownevent)
            dequeue!(s.scheduledevents, g.lockdownevent)
            # if r.group.lockdownevent==1
            #     println("I=", r.group.I, " t=", s.t, "Lockdown event ", r.group.lockdownevent,
            #     " de-scheduled")
            # end
        end
    end
    if g.I == g.Ithreshdown
        enqueue!(s.scheduledevents, g.liftlockdownevent, s.t + s.τsafety)
        # if r.group.lockdownevent==1
        #     println("I=", r.group.I, " t=", s.t, "LiftLockdown event ", r.group.liftlockdownevent,
        #     " scheduled for ", s.t + s.τsafety)
        # end
    end
    return true
end r.group.I s.Itotal s.Itotallockdown

# Rate of the recovery reaction in a group
@rate Population Recovery begin
    return s.k*r.group.I
end r.group.I

# Definition of the event that initiates lockdown in a group
@event Population ActivateLockdown begin
    if r.group.isinlockdown==0
        s.Itotal -= r.group.I
        s.Itotallockdown += r.group.I
    end
    r.group.isinlockdown = 1
    # if r.group.lockdownevent==1
    #     println("I=", r.group.I, " t=", s.t, "Lockdown event ", r.group.lockdownevent,
    #     " executed")
    # end
    return true
end r.group.isinlockdown s.Itotal s.Itotallockdown

# Definition of the event that removes the lockdown
@event Population LiftLockdown begin
    if r.group.isinlockdown==1
        s.Itotal += r.group.I
        s.Itotallockdown -= r.group.I
    end
    r.group.isinlockdown = 0
    # if r.group.lockdownevent==1
    #     println("I=", r.group.I, " t=", s.t, "LiftLockdown event ", r.group.liftlockdownevent,
    #     " executed")
    # end
    return true
end r.group.isinlockdown s.Itotal s.Itotallockdown



# Functions returning the full state of the system.
# See documentation of these functions in GeneralizedStochasticSimulations
# for details.

function GeneralizedStochasticSimulations.getfullstate(sp::Population)
    state = Dict{String, Any}()
    for s in [:S :I :isinlockdown]
        state[String(s)] = similar(sp.groups,fieldtype(eltype(sp.groups),s))
    end
    getfullstate!(state, sp)
    return state
end

function GeneralizedStochasticSimulations.getfullstate!(state::Dict{String, Any}, sp::Population)
    for s in [:S :I :isinlockdown]
        a = state[String(s)]
        for (i,g) in enumerate(sp.groups)
            a[i] = getfield(g,s)
        end
    end
    for s in [:Itotal :Itotallockdown]
        state[String(s)] = getfield(sp,s)
    end
end


end
