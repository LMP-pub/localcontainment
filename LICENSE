#############################################################################
#           License for the code contained in this repository               #
#############################################################################

MIT License

Copyright (c) 2020 Philip Bittihn, Jonas Isensee

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.


#############################################################################
#  Licenses for population and infection data contained in the simulation   #
#                     subdirectory of this repository                       #
#############################################################################


-----------------------------------------------------------------------------
Germany: Data collected by the Robert-Koch-Institut (RKI) and distributed
under the "Data licence Germany – attribution – Version 2.0" or
"dl-de/by-2-0", licence text available at www.govdata.de/dl-de/by-2-0
-----------------------------------------------------------------------------
-----------------------------------------------------------------------------
Italy:
Population Data
Instituto Nazionale di Statistica
http://dati.istat.it/Index.aspx?QueryId=18460&lang=en

Infection Data
Dipartimento della Protezione Civile
Creative Commons Attribution 4.0 International
https://github.com/pcm-dpc/COVID-19
-----------------------------------------------------------------------------
-----------------------------------------------------------------------------
England:
Population Data
Office for National Statistics - Population of the UK by country of birth and nationality 
January 2019 to December 2019
https://www.ons.gov.uk/peoplepopulationandcommunity/populationandmigration/internationalmigration/datasets/populationoftheunitedkingdombycountryofbirthandnationality
Office for National Statistics, Social Survey Division. (2020).
Annual Population Survey, 2004-2019: Secure Access. [data collection].
16th Edition. UK Data Service. SN: 6721, http://doi.org/10.5255/UKDA-SN-6721-15

Infection data
https://coronavirus.data.gov.uk/
-----------------------------------------------------------------------------
-----------------------------------------------------------------------------
New York State:
Population Data
U.S. Census Bureau, "Annual Estimates of the Resident Population for Counties: April 1, 2010 to July 1, 2019,"
accessed July 08, 2020, https://www.census.gov/data/tables/time-series/demo/popest/2010s-counties-total.html

Infection Data
COVID-19 Data Repository by the Center for Systems Science and Engineering (CSSE) at Johns Hopkins University
https://github.com/CSSEGISandData/COVID-19
(distribution of this data with the manuscript is prohibited, so it is downloaded later by the provided scripts)
-----------------------------------------------------------------------------
-----------------------------------------------------------------------------
