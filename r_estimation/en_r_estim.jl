using DataFrames, CSV, Query, Dates

cd(@__DIR__)
download("https://coronavirus.data.gov.uk/downloads/csv/coronavirus-cases_latest.csv",
    "uk_data_latest.csv")
full_df = CSV.read("uk_data_latest.csv")
full_df[:, :region] = full_df[:,Symbol("Area name")]
full_df[:, :region_type] = full_df[:,Symbol("Area type")]
full_df[:, :date] = full_df[:,Symbol("Specimen date")]
full_df[:, :cases] = full_df[:,Symbol("Daily lab-confirmed cases")]
df = full_df |>
    @filter(_.region_type == "Upper tier local authority") |>
    DataFrame

################################################################################
#                 Estimate R_lockdown for the whole country                    #
################################################################################
include("estimate_reproduction_factor.jl")
# Data is not available every day before March 1st
daily_cases = full_df |>
    @filter(_.region == "England" && (Date(2020, 03, 01) <= _.date <= Date(2020,06,12))) |>
    @orderby(_.date) |>
    @select(:date, :cases) |>
    DataFrame

lockdown_R, estim = estimate_lockdown_R(daily_cases.cases)

println("Lockdown R value is $lockdown_R")


################################################################################
#                            Plot History of R                                 #
################################################################################

using Plots
pyplot()

R = estim[:R][!,Symbol("Mean(R)")]
σR = estim[:R][!,Symbol("Std(R)")]
dates = daily_cases.date[15:end]

rmean10 = map(1:(length(R)-10)) do i
    mean(R[i:i+9])
end
idx = sortperm(rmean10)[1]
ldR = rmean10[idx]
lowdates = dates[idx:idx+9]

d1 = minimum(lowdates) - Day(5)
d2 = maximum(lowdates) + Day(5)
da = Date(2020, 4, 25)


p = plot(dates, R, ribbon=σR, ylims=(0.4,1.7),
    lw=1,
    ylabel="R", label="estimate",
    titleloc=:center, size=(400,300),
    yticks = 0.5:0.3:2.0,
    legend=false,
    )
plot!([d1, d2], [ldR, ldR], color=:red,label=PyPlot.L"R_l ="*"$(round(ldR, digits=2))")
scatter!(lowdates, R[idx], color=:red, label="", markerstrokewidth=0.0)
plot!([dates[1],dates[end]], [1, 1], color=:black, label="", ls=:dash)
xlims!(Dates.value(first(dates)), Dates.value(last(dates)))
annotate!((d1,ldR-0.15, PyPlot.latexstring("R_L =$(round(ldR, digits=2))"), :left))
annotate!((d1,1.1,PyPlot.L"R=1.0", :left))
savefig("r_estimate_uk_$(today()).pdf")
PyPlot.show(block=false)
