using RCall, Statistics
# https://www.rdocumentation.org/packages/EpiEstim/versions/2.2-1
@rlibrary EpiEstim


function estimate_lockdown_R(daily_cases)
    method = "parametric_si"
    windowsize = 14
    numdays = length(daily_cases)

    # Serial Interval Data taken from He, X. et al. Temporal dynamics in viral shedding and transmissibility of COVID-19.Nature133Medicine26, 672–675 (2020)
    # Std Deviation of the Gamma Distribution was not given. Instead it was recomputed from the fit parameters.

    config = Dict(pairs(
            (mean_si=5.8, std_si=4.7,                         # for parametric serial interval
            n1 = 1000, n2 = 1000,                             # sample size of SI distributions to draw
            t_start = 2:numdays - (windowsize - 1),           # time window start times
            t_end = (windowsize + 1):numdays))...)

    cfg = make_config(config)
    estim =  rcopy(estimate_R(daily_cases; method=method, config=cfg))

    R = estim[:R][!,Symbol("Mean(R)")]
    rmean10 = map(1:(length(R)-10)) do i
        mean(R[i:i+9])
    end
    return minimum(rmean10), estim
end
