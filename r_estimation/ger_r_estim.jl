using DataFrames, CSV, Query, Dates

cd(@__DIR__)
download("https://www.arcgis.com/sharing/rest/content/items/f10774f1c63e40168479a1feb6c7ca74/data",
    "RKI_COVID19_$(today()).csv")
full_df = CSV.read("RKI_COVID19_$(today()).csv")

################################################################################
#                 Estimate R_lockdown for the whole country                    #
################################################################################
include("estimate_reproduction_factor.jl")
# Data is not available every day before March 1st
df = full_df |>
    @rename(:Meldedatum => :date, :AnzahlFall => :cases, :Landkreis => :region,
            :AnzahlGenesen => :recovered) |>
    @mutate(date = Date(String(_.date), dateformat"yyyy/mm/dd H:M:S")) |>
    @select(:date, :cases, :region, :recovered) |>
    @groupby({_.date, _.region}) |>
    @map({date = key(_)[1],
            region=key(_)[2],
            cases = sum(_.cases),
            recovered = sum(_.recovered)}) |>
    @filter(Date(2020, 03, 01) <= _.date <= Date(2020,06,12)) |>
    @orderby(_.date) |>
    DataFrame

daily_cases = df |>
    @groupby(_.date) |>
    @map({date=key(_), cases=sum(_.cases)}) |>
    @orderby(_.date) |>
    DataFrame
lockdown_R, estim = estimate_lockdown_R(daily_cases.cases)

println("Lockdown R value is $lockdown_R")

################################################################################
#                            Plot History of R                                 #
################################################################################

using Plots
pyplot()

R = estim[:R][!,Symbol("Mean(R)")]
σR = estim[:R][!,Symbol("Std(R)")]
dates = daily_cases.date[15:end]

rmean10 = map(1:(length(R)-10)) do i
    mean(R[i:i+9])
end
idx = sortperm(rmean10)[1]
ldR = rmean10[idx]
lowdates = dates[idx:idx+9]
d1 = minimum(lowdates) - Day(5)
d2 = maximum(lowdates) + Day(5)
da = Date(2020, 4, 25)


p = plot(dates, R, ribbon=σR, ylims=(0.4,1.7),
    lw=1,
    ylabel="R", label="estimate",
    titleloc=:center, size=(400,300),
    yticks = 0.5:0.3:2.0,
    legend=false,
    )
plot!([d1, d2], [ldR, ldR], color=:red,label=PyPlot.L"R_l ="*"$(round(ldR, digits=2))")
scatter!(lowdates, R[idx:idx+9], color=:red, label="", markerstrokewidth=0.0)
plot!([dates[1],dates[end]], [1, 1], color=:black, label="", ls=:dash)
xlims!(Dates.value(first(dates)), Dates.value(last(dates)))
annotate!((d1,ldR-0.15, PyPlot.latexstring("R_L = $(round(ldR, digits=2))"), :left))
annotate!((d1,1.1,PyPlot.L"R=1.0", :left))
savefig("r_estimate_germany_$(today()).pdf")
PyPlot.show(block=false)
