using DataFrames, CSV, Query, Dates

state = "Florida"
#state = "New York"

cd(@__DIR__)
download("https://raw.githubusercontent.com/CSSEGISandData/COVID-19/master/csse_covid_19_data/csse_covid_19_time_series/time_series_covid19_confirmed_US.csv",
    "us_data_latest.csv")
loaded_df = CSV.read("us_data_latest.csv")
loaded_df = stack(loaded_df, 12:ncol(loaded_df))
loaded_df.variable = Date.(String.(loaded_df.variable), dateformat"mm/dd/yy").+Year(2000)
gdf = groupby(loaded_df, [:Admin2, :Combined_Key, :FIPS, :Province_State])

cmbnd = combine(gdf, :value =>  :cases, :variable => :date)
cmbnd[!, :countyname] = cmbnd.Admin2 .* " County"
filter!(r-> r.Province_State == state, cmbnd)

################################################################################
#                 Estimate R_lockdown for the whole country                    #
################################################################################
include("estimate_reproduction_factor.jl")
# Data is not available every day before March 1st
state_cases = cmbnd |>
    @filter(_.Province_State == state && Date(2020, 03, 01) <= _.date <= Date(2020,06,12)) |>
    @orderby(_.date) |>
    @groupby(_.date) |>
    @map({date = key(_), cases=sum(_.cases)}) |>
    DataFrame

daily_cases = state_cases.cases[2:end] .- state_cases.cases[1:end-1]
i = findfirst(x-> x>0, daily_cases)
daily_cases = daily_cases[i:end]
lockdown_R,estim = estimate_lockdown_R(daily_cases)

println("Lockdown R value is $lockdown_R")

################################################################################
#                            Plot History of R                                 #
################################################################################

using Plots
pyplot()

R = estim[:R][!,Symbol("Mean(R)")]
σR = estim[:R][!,Symbol("Std(R)")]
dates = state_cases.date[i+15:end]

rmean10 = map(1:(length(R)-10)) do i
    mean(R[i:i+9])
end
idx = sortperm(rmean10)[1]
ldR = rmean10[idx]
lowdates = dates[idx:idx+9]

d1 = minimum(lowdates) - Day(5)
d2 = maximum(lowdates) + Day(5)
da = Date(2020, 4, 25)


p = plot(dates, R, ribbon=σR, ylims=(0.4,1.7),
    lw=1,
    ylabel="R", label="estimate",
    titleloc=:center, size=(400,300),
    yticks = 0.4:0.3:2.0,
    legend=false,
    )
plot!([d1, d2], [ldR, ldR], color=:red,label=PyPlot.L"R_l ="*"$(round(ldR, digits=2))")
scatter!(lowdates, R[idx], color=:red, label="", markerstrokewidth=0.0)
plot!([dates[1],dates[end]], [1, 1], color=:black, label="", ls=:dash)
xlims!(Dates.value(first(dates)), Dates.value(last(dates)))
annotate!((d1,ldR-0.15,PyPlot.latexstring("R_L =$(round(ldR, digits=2))"), :left))
annotate!((d1,1.1,PyPlot.L"R=1.0", :left))
savefig("r_estimate_us_$(state)_$(today()).pdf")
PyPlot.show(block=false)
